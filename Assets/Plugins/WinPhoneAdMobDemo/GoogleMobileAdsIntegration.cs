﻿using UnityEngine;
using Prime31;
using Prime31.WinPhoneAdMob;

public class GoogleMobileAdsIntegration : MonoBehaviourGUI
{
#if UNITY_WP8

    public string admobID;

    public void ShowBanner()
    {
        WinPhoneAdMob.createBanner(admobID, AdFormat.Banner, AdHorizontalAlignment.Center, AdVerticalAlignment.Bottom, true);
    }

    public void HideBanner()
    {
        WinPhoneAdMob.removeBanner();
    }

#endif
}
