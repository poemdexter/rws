﻿using UnityEngine;
using System.Collections;

public class HazardMovement : MonoBehaviour
{
    private float moveSpeed = 0;

    public void SetSpeed(float speed)
    {
        moveSpeed = speed;
    }
    
    void Update()
    {
        transform.Translate(-Vector2.right * moveSpeed * Time.deltaTime);
    }
}
