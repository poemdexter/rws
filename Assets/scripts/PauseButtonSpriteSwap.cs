﻿using UnityEngine;
using System.Collections;

public class PauseButtonSpriteSwap : MonoBehaviour
{
    private UI2DSprite target;
    public Sprite pause;
    public Sprite play;
    public bool isPaused = false;
    
    void Start()
    {
        target = GetComponent<UI2DSprite>();
        SetPause(isPaused);
    }
    
    public void SwapSprite()
    {
        if (isPaused) {
            isPaused = false;
            target.sprite2D = pause;
        } else {
            isPaused = true;
            target.sprite2D = play;
        }
    }
    
    public void SetPause(bool paused)
    {
        isPaused = paused;
        target.sprite2D = (isPaused) ? play : pause;
    }
}
