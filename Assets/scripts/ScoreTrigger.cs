﻿using System.Globalization;
using UnityEngine;
using System.Collections;

public class ScoreTrigger : MonoBehaviour
{
    public int score { get; private set; }
    UILabel scoreText;
    SpawnHazards spawner;
    bool canScore = true;
    
    void Start()
    {
        spawner = GameObject.FindGameObjectWithTag("GameController").GetComponent<SpawnHazards>();
        scoreText = GameObject.FindGameObjectWithTag("Score").GetComponent<UILabel>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (canScore) {
            score++;
            spawner.IncrementScore();
            scoreText.text = score.ToString();
        }
    }

    public void OnDeath()
    {
        canScore = false;
    }
    
    public void OnReset()
    {
        canScore = true;
        score = 0;
        scoreText.text = score.ToString();
    }
}
