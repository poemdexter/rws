﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SpawnHazards : MonoBehaviour
{
    public GameObject[] hazards;
    public Vector2[] difficulties;
    public int difficultySwitchAmount;
    public static readonly Vector3 POOLED_POSITION = new Vector3(30, 0, 0);
    
    private bool canSpawn = false;
    private float currentTime = 99f;
    private GameObject[] hazardPool;
    private Transform[] spawnpoints;
    private ScrollingBackground scroller;
    private float spawnRate;
    private float gameSpeed;
    private int currentScore = 0;
    private int currentDifficulty = 0;
    
    void Start()
    {
        scroller = GetComponent<ScrollingBackground>();

        AdjustDifficulty();
        scroller.SetSpeed(gameSpeed);
        
        spawnpoints = new Transform[transform.childCount];
        for (int i = 0; i < transform.childCount; i++) {
            spawnpoints[i] = transform.GetChild(i);
        }
        
        hazardPool = new GameObject[hazards.Length];
        for (int i = 0; i < hazardPool.Length; i++) {
            hazardPool[i] = (GameObject)Instantiate(hazards[i], POOLED_POSITION, hazards[i].transform.rotation);
        }
    }
    
    void Update()
    {
        if (canSpawn && (currentTime += Time.deltaTime) > spawnRate) {
            currentTime = 0;
            scroller.SetSpeed(gameSpeed);
            SpawnHazard();
        }
    }
    
    void AdjustDifficulty()
    {
        currentDifficulty = Mathf.FloorToInt(currentScore / difficultySwitchAmount);
        if (currentDifficulty < difficulties.Length) {
            spawnRate = difficulties[currentDifficulty].x;
            gameSpeed = difficulties[currentDifficulty].y;
        }
    }

    void SpawnHazard()
    {
        GameObject hazard = hazardPool[UnityEngine.Random.Range(0, hazardPool.Length)];
        while (hazard.transform.position != POOLED_POSITION) {
            hazard = hazardPool[UnityEngine.Random.Range(0, hazardPool.Length)];
        }
        
        Transform spawnpoint = spawnpoints[UnityEngine.Random.Range(0, spawnpoints.Length)];
        hazard.transform.position = spawnpoint.position;
        hazard.renderer.sortingOrder += Convert.ToInt32(spawnpoint.gameObject.name);
        hazard.GetComponent<HazardMovement>().SetSpeed(gameSpeed);
        hazard.GetComponent<HazardMovement>().enabled = true;
    }
    
    public void IncrementScore()
    {
        currentScore++;
        AdjustDifficulty();
    }
    
    public void OnDeath()
    {
        canSpawn = false;
    }
    
    public void OnReset()
    {
        currentTime = 0;
        currentScore = 0;
        currentDifficulty = 0;
        canSpawn = true;
        AdjustDifficulty();
        scroller.SetSpeed(gameSpeed);
    }
}
