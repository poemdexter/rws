﻿using System.Collections;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private ScrollingBackground background;
    private GameObject deadMenu;
    private GameObject gameMenu;
    private GameObject tapHand;
    private int highScore;
    private bool isPaused;
    private JimmyCollision jimmyCollide;
    private JimmyMovement jimmyMove;
    private ScoreTrigger score;
    private UILabel deadScoreText;
    private UILabel deadHighScoreText;
    private UI2DSprite deadAlert;
    private ScreenShake shake;
    private SpawnHazards spawner;
    private GoogleMobileAdsIntegration googleAds;
    

    private void Start()
    {
        spawner = GetComponent<SpawnHazards>();
        background = GetComponent<ScrollingBackground>();
        var player = GameObject.FindGameObjectWithTag("Player");
        jimmyCollide = player.GetComponent<JimmyCollision>();
        jimmyMove = player.GetComponent<JimmyMovement>();
        shake = player.GetComponent<ScreenShake>();
        score = GameObject.FindGameObjectWithTag("ScoreTrigger").GetComponent<ScoreTrigger>();
        gameMenu = GameObject.FindGameObjectWithTag("GameMenu");
        deadMenu = GameObject.FindGameObjectWithTag("DeadMenu");
        tapHand = GameObject.FindGameObjectWithTag("TapHand");
        googleAds = GameObject.FindGameObjectWithTag("GoogleAds").GetComponent<GoogleMobileAdsIntegration>();

        // dead menu ui elements
        deadScoreText = GameObject.FindGameObjectWithTag("DeadMenuScore").GetComponent<UILabel>();
        deadHighScoreText = GameObject.FindGameObjectWithTag("DeadMenuHighScore").GetComponent<UILabel>();
        deadAlert = GameObject.FindGameObjectWithTag("DeadMenuAlert").GetComponent<UI2DSprite>();

        // hide menus
        gameMenu.SetActive(false);
        deadMenu.SetActive(false);

        if (PlayerPrefs.HasKey("HighScore"))
        {
            var i = PlayerPrefs.GetInt("HighScore");
            highScore = i;
        }
        else
        {
            highScore = 0;
        }

        if (AudioController.IsMusicEnabled())
        {
            AudioController.StopMusic();
            AudioController.PlayMusic("MainThemeIntro");
        }

        tapHand.SetActive(true);
        StartCoroutine(ShowTapTutorial());
    }

    private void Update()
    {
        // Android Back Button (open for cheating via slow mo pause/unpause)
        // Windows Phone, manditory and will kick player back to main menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                UnPauseGame();
            }
            else
            {
                Application.LoadLevel(0);
            }
            
        }
    }

    IEnumerator ShowTapTutorial()
    {
        yield return StartCoroutine(Wait(1.0f));

        // do 3 times
        for (int i = 0; i < 4; i++)
        {
            if (i % 2 == 0)
                jimmyMove.SwapPosition();
            tapHand.GetComponent<PauseButtonSpriteSwap>().SwapSprite();
            for (float timer = 0; timer < .7f; timer += Time.deltaTime) // wait 1 sec between
                yield return 0;
        }

        tapHand.SetActive(false);
        spawner.OnReset();
    }

    IEnumerator Wait(float duration)
    {
        for (float timer = 0; timer < duration; timer += Time.deltaTime)
            yield return 0;
    }

    // event fall through lands here
    public void OnPress(bool isDown)
    {
        if (isPaused && isDown)
        {
            UnPauseGame();
        }
        else
        {
            jimmyMove.TryToMove();
        }
    }

    // on game background or lose focus, pause it
    private void OnApplicationFocus(bool pauseStatus)
    {
        if (pauseStatus)
            SetPauseGame();
    }

    public void TogglePauseGame()
    {
        if (!isPaused)
        {
            SetPauseGame();
        }
        else
        {
            UnPauseGame();
        }
    }

    private void UnPauseGame()
    {
        isPaused = false;
        Time.timeScale = 1;
        AudioController.UnpauseAll();
        gameMenu.SetActive(false);
        jimmyMove.UnPause();
        shake.UnPause();
        
    }

    private void SetPauseGame()
    {
        if (!deadMenu.activeSelf)
        {
            isPaused = true;
            AudioController.PauseAll();
            gameMenu.SetActive(true);
            Time.timeScale = 0;
            jimmyMove.Pause();
            shake.Pause();
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ShowGameOver()
    {
        CheckNewHighScore();
        deadMenu.SetActive(true);
        googleAds.ShowBanner();
    }

    void CheckNewHighScore()
    {
        var s = score.score;
        if (s > highScore)
        {
            highScore = s;
            PlayerPrefs.SetInt("HighScore", s);
            PlayerPrefs.Save();
            deadAlert.enabled = true;
        }
        else
        {
            deadAlert.enabled = false;
        }
        deadScoreText.text = s.ToString();
        deadHighScoreText.text = highScore.ToString();

    }

    public void PlayerDeath()
    {
        spawner.OnDeath();
        jimmyCollide.OnDeath();
        jimmyMove.OnDeath();
        score.OnDeath();
        background.OnDeath();
    }

    public void GameReset()
    {
        deadMenu.SetActive(false);
        googleAds.HideBanner();
        spawner.OnReset();
        jimmyCollide.OnReset();
        jimmyMove.OnReset();
        score.OnReset();
    }
}