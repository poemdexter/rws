﻿using UnityEngine;
using System.Collections;

public class HazardDestroyTrigger : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        col.gameObject.transform.position = SpawnHazards.POOLED_POSITION;
        col.gameObject.renderer.sortingOrder = 2;
        col.gameObject.GetComponent<HazardMovement>().enabled = false;
    }
}
