﻿using UnityEngine;
using System.Collections;

public class FireParticles : MonoBehaviour
{
    ParticleSystem bloodParticles;
    
    void Start()
    {
        bloodParticles = GameObject.FindGameObjectWithTag("BloodParticles").GetComponent<ParticleSystem>();
    }
    
    public void FireBloodParticles()
    {
        bloodParticles.Emit(30);
    }
}
