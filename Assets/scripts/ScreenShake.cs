﻿using UnityEngine;
using System.Collections;

public class ScreenShake : MonoBehaviour
{
    float shake;
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;
    Vector3 origPosition;
    private bool paused = false;
    
    void Start()
    {
        origPosition = Camera.main.transform.position;
    }
    
    public void Shake()
    {
        shake = shakeAmount;
    }
    
    void Update()
    {
        if (!paused) {
            if (shake > 0) {
                Vector3 pos = Random.insideUnitSphere * shake;
                Camera.main.transform.position = origPosition + new Vector3(pos.x, pos.y, 0); 
                shake -= Time.deltaTime * decreaseFactor;
            } else {
                Camera.main.transform.position = origPosition;
                shake = 0;  
            }
        }
    }
    
    public void Pause()
    {
        paused = true;
    }
    
    public void UnPause()
    {
        paused = false;
    }
}