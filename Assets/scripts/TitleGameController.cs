﻿using UnityEngine;

public class TitleGameController : MonoBehaviour
{
    private ScrollingBackground background;
    private PauseButtonSpriteSwap audioButton;
    private AudioController audioController;

    private bool soundEnabled = true;
    private int highScore = 0;

    void Start()
    {
        audioButton = GameObject.FindGameObjectWithTag("AudioButton").GetComponent<PauseButtonSpriteSwap>();
        audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
        DontDestroyOnLoad(audioController);
        background = GetComponent<ScrollingBackground>();
        background.SetSpeed(20);
        
        if (PlayerPrefs.HasKey("SoundEnabled")) {
            string s = PlayerPrefs.GetString("SoundEnabled");
            
            if (s == "OFF")
            {
                audioButton.isPaused = true;
                ToggleSound();
            }
        } else {
            PlayerPrefs.SetString("SoundEnabled", "ON");
            PlayerPrefs.Save();
        }

        if (PlayerPrefs.HasKey("HighScore"))
        {
            int i = PlayerPrefs.GetInt("HighScore");
            highScore = i;   
        }
        GameObject.FindGameObjectWithTag("HighScore").GetComponent<UILabel>().text = "Best: " + highScore;
    }

    private void Update()
    {
        // Windows Phone, manditory and will kick player out of game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
                Application.Quit();
        }
    }
    
    public void ToggleSound()
    {
        soundEnabled = !soundEnabled;
        if (soundEnabled) {
            audioController.DisableAudio = false;
            AudioController.EnableMusic(true);
            AudioController.PlayMusic("IntroFanfare");
            PlayerPrefs.SetString("SoundEnabled", "ON");
            PlayerPrefs.Save();
        } else {
            audioController.DisableAudio = true;
            AudioController.StopAll();
            PlayerPrefs.SetString("SoundEnabled", "OFF");
            PlayerPrefs.Save();
        }
    }

    public void StartGame()
    {
        Application.LoadLevel(1);
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }

    public void ShowPrivacyPolicy()
    {
        Application.OpenURL("http://poemdexter.com/misc/privacy-policy.html");
    }
    
    public void ResetScore()
    {
        PlayerPrefs.SetInt("HighScore", 0);
        PlayerPrefs.Save();
        highScore = 0;
        GameObject.FindGameObjectWithTag("HighScore").GetComponent<UILabel>().text = "Best: " + highScore;
    }
}
