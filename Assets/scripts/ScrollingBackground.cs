﻿using UnityEngine;
using System.Collections;
using Prime31.GoKitLite;

public class ScrollingBackground : MonoBehaviour
{
    public GameObject backgroundTile;
    public int tileAmount;
    public float tileWidth;
    private float backgroundSpeed = 0;
    private float OriginalSpeed { get; set; }
    private GameObject[] backgroundPool;
    private int leftBackgroundTile;
    private int rightBackgroundTile;
    private int wrapXLimit;
    private int hazardSpawnCount;

    void Start()
    {
        backgroundPool = new GameObject[tileAmount];
        for (int j = 0; j<backgroundPool.Length; j++) {
            backgroundPool[j] = (GameObject)Instantiate(backgroundTile);
        }
        
        wrapXLimit = Mathf.CeilToInt(tileAmount / 2);
        for (int j = 0; j < tileAmount; j++) {
            backgroundPool[j].transform.position = new Vector2((j - wrapXLimit) * tileWidth, backgroundPool[j].transform.position.y);
        }
        leftBackgroundTile = 0;
        rightBackgroundTile = tileAmount;
    }
    
    void Update()
    {
        for (int j = 0; j < tileAmount; j++) {
            backgroundPool[j].transform.Translate(-Vector2.right * backgroundSpeed * Time.deltaTime);
        }
    
        Vector2 bgPos = backgroundPool[leftBackgroundTile].transform.position;
        if (bgPos.x < -wrapXLimit * tileWidth) {
            backgroundPool[leftBackgroundTile].transform.position = bgPos + (Vector2.right * (tileWidth * (backgroundPool.Length)));
            leftBackgroundTile = (leftBackgroundTile + 1 < backgroundPool.Length) ? leftBackgroundTile + 1 : 0;
            rightBackgroundTile = (rightBackgroundTile + 1 < backgroundPool.Length) ? rightBackgroundTile + 1 : 0;
        }
    }
    
    public void SetSpeed(float speed)
    {
        backgroundSpeed = speed;
        OriginalSpeed = backgroundSpeed;
    }
    
    public void SetTweenSpeed(float speed)
    {
        backgroundSpeed = speed;
    }
    
    public void OnDeath()
    {
        
        GoKitLite.instance.customAction(transform, 1.0f, 
            ( trans, dt ) => {
            float speed = trans.GetComponent<ScrollingBackground>().OriginalSpeed;
            trans.GetComponent<ScrollingBackground>().SetTweenSpeed(speed - (speed * dt));
        }, .63f, GoKitLiteEasing.Sinusoidal.EaseIn);
    }
}
