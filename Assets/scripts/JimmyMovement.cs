﻿using UnityEngine;
using System.Collections;

public class JimmyMovement : MonoBehaviour
{
    public Vector3 upPosition;
    public Vector3 downPosition;

    private bool isUp = true;
    private bool canMove = true;
    private bool dead = false;
    private bool moved = false;
    
    public void TryToMove()
    {
        moved = false;
        
        if (canMove && !dead) {
            
            // taps
            foreach (Touch touch in Input.touches) {
                if (!moved && touch.phase == TouchPhase.Began) {
                    SwapPosition();
                    moved = true;
                }
            }
        }
    }

    public void SwapPosition()
    {
        if (isUp) {
            isUp = false;
            transform.position = downPosition;
            renderer.sortingOrder += 1;
        } else {
            isUp = true;
            transform.position = upPosition;
            renderer.sortingOrder -= 1;
        }
    }

    public void Pause()
    {
        canMove = false;
    }
    
    public void UnPause()
    {
        canMove = true;
    }
    
    public void OnDeath()
    {
        canMove = false;
        dead = true;
    }
    
    public void OnReset()
    {
        canMove = true;
        dead = false;
    }
}
