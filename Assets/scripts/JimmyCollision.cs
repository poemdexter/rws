﻿using UnityEngine;
using System.Collections;

public class JimmyCollision : MonoBehaviour
{
    private Animator anim;
    private GameController gc;
    private bool isDead = false;
    
    void Start()
    {
        anim = GetComponent<Animator>();
        gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }
	
    void OnTriggerEnter(Collider col)
    {
        if (!isDead) {
            AudioController.Play("trip");
            gc.PlayerDeath();
            isDead = true;
        }
    }
    
    public void PlayLandingSound()
    {
        AudioController.Play("landing");
        Handheld.Vibrate();
    }
    
    public void DeathAnimationFinished()
    {
        gc.ShowGameOver();
    } 
    
    public void OnDeath()
    {
        anim.SetTrigger("Falls");
    }
    
    public void OnReset()
    {
        anim.SetTrigger("Reset");
        isDead = false;
    }
}
